import random
import asyncio
from aiohttp import web


async def timer(id):
    try:
        print(f'timer {id} started')
        await asyncio.sleep(30)
    except asyncio.CancelledError:
        print(f'timer {id} cancelled')
    else:
        print(f'timer {id} finished')


async def start_timer(request):
    loop = asyncio.get_event_loop()
    id = random.randint(0, 10)
    loop.create_task(timer(id))
    return web.Response(text=str(id))


async def stop_timer(request):
    timer_id = int(request.match_info.get('id'))
    active_tasks = [task for task in asyncio.all_tasks() if not task.done()]
    for task in active_tasks:
        coro = task.get_coro()
        if coro.__name__ == 'timer' and coro.cr_frame.f_locals['id'] == timer_id:
            task.cancel()
            break

    return web.Response()


if __name__ == '__main__':
    app = web.Application()
    app.router.add_get('/start/', start_timer)
    app.router.add_get('/stop/{id}/', stop_timer)
    web.run_app(app)
